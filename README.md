# Práctica FDV Eventos #

En esta actividad realizaremos pruebas con las diferentes modalidades de uso de eventos para enviar mensajes entre los objetos de la escena en Unity.

En Unity se pueden definir:

# Eventos de la UI
# Eventos del sistema de eventos soportados
# Nuestros propios eventos:
# Sistema de mensajes  de Unity
# Delegados, eventos C# - Patrón observador.


## Actividades a realizar:

### 1.Implementar una aproximación a la UI que hayas establecido para tu prototipo.

### 2.Determinar los eventos que requiere tu prototipo.

### 3.Implementar un GameController que ante un determinado evento que se produzca en el juego, dos tipos de objetos diferentes se comporten de forma diferente.

### 4.Agregar a tu escena un objeto que al ser recolectado por el jugador desplacen del juego un tipo de objetos que puedan representar obstáculos, a modo de barreras que abren caminos.